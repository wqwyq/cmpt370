/*
 *	Alexander Shmakov
 *	cmpt370::assign2
 *	Oct 26,2016
 */

import java.util.*;
import java.io.*;
import java.nio.file.*;


/*
 *	This programm is used to generate data from tests on the ratios of max depth of the Btree and AVL trees
 */
public class Trees {

	/*
	 *	This is the main method of the program
	 */
	public static void main(String[] args) {

		ArrayList<int[]> origArrays = new ArrayList<int[]>();
		ArrayList<int[]> sortedArrays = new ArrayList<int[]>();

		Node head = null;
		double depth = 0;
		int jumps = 5000, lines = 100;

		String fileName = "depths.csv", fileName2 = "depthsSorted.csv";


		//generating arrays to build trees with
		for (int i = 1; i <= lines; i++) {
			int[] array = genArray(jumps * i);
			origArrays.add(array);

			//creating sorted arrays of the existing onces
			int[] temp = Arrays.copyOf(array,array.length);
			Arrays.sort(temp);
			sortedArrays.add(temp);
		}


		try {

			Files.write(Paths.get(fileName), "size,Binary-tree/AVL-tree\n".getBytes());
			Files.write(Paths.get(fileName2), "size,Binary-tree/AVL-tree\n".getBytes());
			System.out.println("size,Binary-tree/AVL-tree");

			for(int j=1; j <= lines; j++) {

				Files.write(Paths.get(fileName), (""+(j*jumps)+",").getBytes(), StandardOpenOption.APPEND);
				System.out.print(""+(j*jumps)+",");

				int[] tempArr = origArrays.get(j-1);

				head = null;
				for(int i=0; i<tempArr.length; i++) {
					head = insertNode(head, tempArr[i]);
				}

				depth = depthFinder(head, 0);

				head = null;
				for(int i=0; i<tempArr.length; i++) {
					head = avlInsertNode(head, tempArr[i]);
				}

				depth = depth/depthFinder(head, 0);

				Files.write(Paths.get(fileName), ("" + depth + "\n").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + depth + "\n");

				//################ sorted array part
				depth = 0;

				Files.write(Paths.get(fileName2), (""+(j*jumps)+",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("Sorted "+(j*jumps)+",");

				tempArr = sortedArrays.get(j-1);

				head = null;
				for(int i=0; i<tempArr.length; i++) {
					head = insertNode(head, tempArr[i]);
				}

				depth = depthFinder(head, 0);

				head = null;
				for(int i=0; i<tempArr.length; i++) {
					head = avlInsertNode(head, tempArr[i]);
				}

				depth = depth/depthFinder(head, 0);

				Files.write(Paths.get(fileName2), ("" + depth + "\n").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + depth + "\n");

				depth =0 ;
			}
		}
		catch(Exception e) {
			System.out.println(""+e);
		}

	}



	/*
	 *	This method is used to generate a binary tree
	 *	uses a node ponter as a root in each subtree as it recourses, and newNode, as the 
	 *	actual value that is going to be interted into the new node
	 *	It return the node to the previous function call.
	 */
	public static Node insertNode(Node first, int newNode) {
		
		//if the next node is empty, we assign the new node
		if(first == null) {
			first = new Node(newNode);
			// System.out.println("added node: "+newNode);
			return first;
		}

		//in case the node already exists we skip this node
		if( first.getNumber() == newNode) {
			// System.out.println("skipped node: "+newNode);
			return first;
		}

		//if the new node is smaller than the parent we store it in the left side
		if( first.getNumber() > newNode) {
			// System.out.println("recurse LEFT");
			first.setLeft( insertNode(first.getLeft(), newNode) );
		}

		//if the new node is greater than the parent we store it in the right side
		if( first.getNumber() < newNode) {
			// System.out.println("recurse RIGHT");
			first.setRight( insertNode(first.getRight(), newNode) );
		}

		return first;
	}

    /*
     *  This method is used to generate an AVL tree
     *	uses a node ponter as a root in each subtree as it recourses, and newNode, as the 
	 *	actual value that is going to be interted into the new node
	 *	It return the node to the previous function call, and after each step it checks if the
	 *	tree needs to be rotated.
     */
    public static Node avlInsertNode(Node first, int newNode) {

    	//a temporary node to store the first trailing node of a parent node
        Node t = null;

        //storing a new node in the empt spot
        if(first == null) {
            first = new Node(newNode);
            first.findHeight();
            // System.out.println("adding new node");
            return first;
        }

        //skip existing nodes
        if( first.getNumber() == newNode) {
        	// System.out.println("skip, already exists");
            return first;
        }

        //store node on left side of parent
        if( first.getNumber() > newNode) {
            // System.out.println("put in left");
            t = avlInsertNode(first.getLeft(), newNode);
            //storing the new node
            first.setLeft(t);

            // assigning a height to the parent node
            first.findHeight();
        }

        //store node on right side of parent
        else if( first.getNumber() < newNode) {
        	// System.out.println("put in right");
            t = avlInsertNode(first.getRight(), newNode);
            //storing the new node
            first.setRight(t);

            // assigning a height to the parent node
            first.findHeight();
        }

        //makes sure the parent node has children before trying to rotate the tree
        if(first != null) {

        	// when the children nodes have a difference >1 or when the right child is null and the parent has height 2+, then we know its 2 of the 4 cases
	    	if( first.getLeft() != null && ((first.getRight()==null && first.getHeight()>=2) || (first.getLeft().getHeight()-first.getRight().getHeight()>1) )) {
	    		/*			Right rotation
	    		 *	  O
	    		 *	 O
	    		 *	O
	    		 */
	    		if(first.getNumber()>newNode && first.getLeft().getNumber()>newNode) {
	    			// System.out.println("LL");
	    			first = rr(first);
	    		}

	    		/*			Double Right Rotation
	    		 *	  O
	    		 *	 O
	    		 *	  O
	    		 */
	    		else if(first.getNumber()>newNode && first.getLeft().getNumber()<newNode) {
	    			// System.out.println("LR");
	    			//first rotating the subtree of the parent
	    			first.setLeft(lr(first.getLeft()));
	    			first = rr(first);
	    		}
	    	}

        	// when the children nodes have a difference >1 or when the left child is null and the parent has height 2+, then we know its 2 of the 4 cases
	    	else if( first.getRight() != null && ((first.getLeft()==null && first.getHeight()>=2) || (first.getRight().getHeight()-first.getLeft().getHeight()>1)) ) {
	    		/*
	    		 *	O
	    		 *	 O
	    		 *	  O
	    		 */
	    		if(first.getNumber()<newNode && first.getRight().getNumber()<newNode) {
	    			// System.out.println("RR");
	    			first = lr(first);
	    		}

	    		/*		Double left Rotation
	    		 *	O
	    		 *	 O
	    		 *	O
	    		 */
	    		if(first.getNumber()<newNode && first.getRight().getNumber()>newNode) {
	    			// System.out.println("RL");	
					//first rotating the subtree of the parent
	    			first.setRight(rr(first.getRight()));
	    			first = lr(first);
	    		}
	    	}
        }

	    return first;
	}


	/*
	 *	This function performes a right rotation
	 *	It takes a parent node and rotates the tree
	 *	returns a different node as a result
	 */
	private static Node rr(Node node) {
		//store the left node to not lose the pointer
		Node left = node.getLeft();

		//rotating the nodes		
		node.setLeft(left.getRight());
		left.setRight(node);
		
		//changing the heihts of the nodes as a result of the rotation
		node.findHeight();
		left.findHeight();
		
		return left;
	}


	/*
	 *	This function performes a left rotation
	 *	It takes a parent node and rotates the tree
	 *	returns a different node as a result
	 */
	private static Node lr(Node node) {
		//store the left node to not lose the pointer
		Node right = node.getRight();

		//rotating the nodes
		node.setRight(right.getLeft());
		right.setLeft(node);

		//changing the heihts of the nodes as a result of the rotation
		node.findHeight();
		right.findHeight();
		
		return right;
	}


	/*
	 *	This function prints out the entire tree
	 *	it shows each parent node and its children nodes
	 */
	public static void printTree(Node head, int depth) {
		if(head!= null) {
			System.out.println("Depth " +depth+ ": "+ head);
			printTree(head.getLeft(), depth+1);
			printTree(head.getRight(), depth+1);
		}
		return;
	}


	/*
	 *	This function is recursively going through the tree and finds
	 *	the longest path in order to determine the max depth
	 */
	public static int depthFinder(Node head, int counter) {
		int maxDepth = counter, temp1=0, temp2=0;

		if(head!= null) {
			temp1 = depthFinder(head.getLeft(), counter+1);
			temp2 = depthFinder(head.getRight(), counter+1);
			if(temp1>=temp2)
				maxDepth = temp1;
			else
				maxDepth = temp2;
		}
		return maxDepth;	
	}


    /**
     * This method generates an int array with the length passed, and the values between 0 to arrayLength*2
     */
    public static int[] genArray(int arrayLength) {
        
        Random rand = new Random();
        int[] array = new int[arrayLength];

        //fill the array with variables up to twice the length of the array itself
        for(int i=0; i<array.length; i++)
            array[i] = rand.nextInt(arrayLength*2);

        return array;
    }
}