/*
 *	This class represents a Node, that has a value, and left and right pointer to decendants
 */

class Node {

    private int number;
    private int height = 0;
    private Node right;
    private Node left;
    
    public Node(int number) {
        this.number = number;
        right = null;
        left = null;
    }
    
    public int getNumber(){
        return number;
    }

    public int getHeight() {
        return height;
    }

    public Node getRight() {
        return right;
    }

    public Node setRight(Node right) {
        this.right = right;
        return right;
    }

    public Node getLeft() {
        return left;
    }
    

    public Node setLeft(Node left) {
        this.left = left;
        return left;
    }

    public void findHeight() {
        int leftHeight = 0;
        int rightHeight = 0;
        
        if(left != null)
            leftHeight = left.getHeight();

        if(right != null)
            rightHeight = right.getHeight();

        this.height = Math.max(leftHeight, rightHeight) + 1;
    }
    
    public String toString() {

        String rs = "Value: " + number;
        if(left != null) rs += ", left = " + left.getNumber() +",";
        else rs += ", left = null, ";
        
        if(right != null) rs += " right = " + right.getNumber();
        else rs += " right = null";
        return rs;
    }
}