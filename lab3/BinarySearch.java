/*
 *	Alexander Shmakov
 *	cmpt370::lab3
 *	Oct 4,2016
 */

import java.util.*;


/*
 *	This class is representing a binary tree
 */
public class BinarySearch {

	/*
	 *	This is the main method of the program
	 */
	public static void main(String[] args) {

		int[] array = null;

		try {
			if(args.length == 0 ) {
				System.out.println("pass amount to programm");
				return;
			}

			array = genArray(Integer.parseInt( args[0] ) );
			System.out.println(Arrays.toString(array));
		}
		catch(Exception e) {
			System.out.println("Error: " + e);
		}

		Node head = null;

		for(int i=0; i<array.length; i++) {
			head = insertNode(head, array[i]);
			// System.out.println("head i="+i+" : "+head.toString());
		}

		System.out.println("\n\nTree:\n");
		printTree(head, 0);
	}

	/*
	 *	This method is used to generate a binary tree
	 *	uses an array to generate the values for the nodes
	 */
	public static Node insertNode(Node first, int newNode) {

		if(first == null) {
			first = new Node(newNode);
			System.out.println("added node: "+newNode);
			return first;
		}

		if( first.getNumber() == newNode) {
			System.out.println("skipped node: "+newNode);
			return first;
		}
		
		if( first.getNumber() > newNode) {
			System.out.println("recurse LEFT");
			first.setLeft( insertNode(first.getLeft(), newNode) );
		}

		if( first.getNumber() < newNode) {
			System.out.println("recurse RIGHT");
			first.setRight( insertNode(first.getRight(), newNode) );
		}

		return first;
	}


	public static void printTree(Node head, int depth) {
		if(head != null) {
			System.out.println("Depth " +depth+ ": "+ head);
			printTree(head.getLeft(), depth+1);
			printTree(head.getRight(), depth+1);
		}
		return;
	}


    /**
     * This method generates an int array with the length passed, and the values between 0 to arrayLength*2
     */
    public static int[] genArray(int arrayLength) {
        
        Random rand = new Random();
        int[] array = new int[arrayLength];

        //fill the array with variables up to twice the length of the array itself
        for(int i=0; i<array.length; i++)
            array[i] = rand.nextInt(arrayLength*2);

        return array;
    }
}