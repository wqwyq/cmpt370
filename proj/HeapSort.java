/*
 *	Alexander Shmakov
 *	cmpt370::assign4
 *	Nov 30, 2016
 */

import java.util.*;
import java.io.*;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;

public class  Sorters {

	public static void main(String[] args) {

		String fileName = "randomTimes.csv";
		String fileName2 = "sortedTimes.csv";
		String fileName3 = "revSortTimes.csv";

		//running time start
		long startTime = 0;
		long endTime = 0;

		ArrayList<int[]> origArrays = new ArrayList<int[]>();
		ArrayList<int[]> reverseStore = new ArrayList<int[]>();
		ArrayList<int[]> sortedArrays = new ArrayList<int[]>();

		int loops = 100, jumps = 10000;

		try {

			//generating arrays to sort
			for (int i = 1; i <= loops; i++) {
				int[] array = genArray(jumps * i);
				origArrays.add(array);
			}


			//##################################
			//#######  Random Arrays  ##########
			//##################################
			
			Files.write(Paths.get(fileName), "size,HeadpSort,mergeSort\n".getBytes());
			System.out.println("size,HeadpSort,mergeSort");

			for(int j =1; j<=loops; j++) {

				Files.write(Paths.get(fileName), (""+(j*jumps)+",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("random "+(j*jumps)+",");

				
				int[] tempArr = origArrays.get(j-1);
				//heap

				startTime = System.currentTimeMillis();
				heapSort(tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");

				// ##################
				//storing sorted arrays for future sorts
				sortedArrays.add(tempArr);
				//storing reversed sorted array for future sorts
				reverseStore.add( reverse(tempArr) );
				// ##################


				tempArr = origArrays.get(j-1);
				//merge

				startTime = System.currentTimeMillis();
				mergeSort(0, tempArr.length-1, tempArr);
				endTime = System.currentTimeMillis();

				Files.write(Paths.get(fileName), ("" + (endTime - startTime) + "\n").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + "\n");

			}


			// ##################################
			// #######  sorted Arrays  ##########
			// ##################################

			Files.write(Paths.get(fileName2), "size,HeadpSort,mergeSort\n".getBytes());
			System.out.println("size,HeadpSort,mergeSort");

			for(int j =1; j<=loops; j++) {

				Files.write(Paths.get(fileName2), (""+(j*jumps)+",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("sorted "+(j*jumps)+",");
				
				int[] tempArr = sortedArrays.get(j-1);
				//heapSort
				startTime = System.currentTimeMillis();
				heapSort(tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName2), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");

				tempArr = sortedArrays.get(j-1);
				//merge
				startTime = System.currentTimeMillis();
				mergeSort(0, tempArr.length-1, tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName2), ("" + (endTime - startTime) + "\n").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + "\n");
			}


			//##################################
			//######  reverse Arrays  ##########
			//##################################

			Files.write(Paths.get(fileName3), "size,HeadpSort,mergeSort\n".getBytes());
			System.out.println("size,HeadpSort,mergeSort");

			for(int j =1; j<=loops; j++) {

				Files.write(Paths.get(fileName3), (""+(j*jumps)+",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("reverse " +(j*jumps)+",");

				int[] tempArr = reverseStore.get(j-1);
				//heapSort
				startTime = System.currentTimeMillis();
				heapSort(tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName3), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");

				tempArr = reverseStore.get(j-1);
				//merge
				startTime = System.currentTimeMillis();
				mergeSort(0, tempArr.length-1, tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName3), ("" + (endTime - startTime) + "\n").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + "\n");

			}			

		}
		catch (Exception e) {
			System.out.println(""+e);
		}

	}


	//reverse array
	public static int[] reverse(int[] array) {
		for(int i = 0; i < array.length; i++) {
			int temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - 1] = temp;
		}
		return array;
	}

// ###################################### Heap Sort #######################################

	public static void heapSort(int[] arr) {
		int size = arr.length-1;
		//build a heap structure starting from the middle index of the tree
			//run the makeMax function with a different tempRoot node
		for(int i = size/2; i>-1; i--)
			makeMax(false, arr, i, size);

		while(size > 0) {
			//replace the first element of the tree with the last
			replace(arr, 0, size);
			
			//restoring the max head property after the replace
				// --size is 'removing' the last element from the tree 
			makeMax(true, arr, 0, --size);
		}
	}

	/*
	 *	This function replaces two lements in an array that is passed to it at the index(s) given
	 */
	public static void replace(int[] arr, int first, int second) {
		//making a temporary place to store a value
		int temp = arr[first];
		//overwrite the first value
		arr[first] = arr[second];
		//overwrite the second value
		arr[second] = temp;
	}

	/*
	 *	This function replaces elements in the passed array such that the finished product is an array with max heap properties
	 */
	public static void makeMax(boolean restore, int[] arr, int tempRoot, int size) {
		//making a variable to keep track of the node node
		int node = 2*tempRoot;

		//used to start from the second node when restoring the max heap in the array		
		if(restore)
			node = 1;

		//while the node in not the last index of the array
		while(size >= node) {
			// grab the index of the greater sibling 
			if(arr[node+1] > arr[node] && node+1 <= size)
				node++;

			if(restore)
				replace(arr, node, tempRoot);

			//if the node has a higher value than the tempRoot, replace them
			if(!restore)
				if(arr[node] > arr[tempRoot])
					replace(arr, node, tempRoot);

			//offset the tempRoot and node, and loop again
			tempRoot = node;
			node = 2*tempRoot;
		}

		//switch the parent nodes if the sub parent is bigger
		if(restore)
			while(arr[tempRoot]>arr[tempRoot/2] && tempRoot>1) {
				replace(arr, tempRoot/2, tempRoot);
				tempRoot = tempRoot/2;
			}
	}


// ###################################### Merge Sort #######################################

public static int[] mergeSort(int min, int max, int[] array){
	
	if (min >= max) {
		return array;
	}

	int temp = 0;
	int maximum = max;
	int minimum = min;
	int mid = (maximum + minimum)/2;
	int lastMin = 0;
	int firstMax = 0;

	mergeSort(min, mid, array);
	mergeSort(mid+1, max, array);
	
	lastMin = mid;
	firstMax = mid+1;
	
	while ( (firstMax <= maximum) && (min <= lastMin) ) {

		if (array[minimum] < array[firstMax])
			minimum++;
		else {
			temp = array[firstMax];

			for (int i = firstMax- 1; i >= minimum; i--)
				array[i+1] = array[i];

			array[minimum] = temp;

			firstMax++;
			lastMin++;
			minimum++;
		}
	}

	return array;
}


	//	This function prints the array, mainly for debuging
	public static void printArr(int[] arr) {
		
		System.out.print("array: [");
		for (int i=0; i<arr.length; i++) {
			System.out.print(""+ arr[i]);
			if(i==arr.length-1)
				break;
			System.out.print(", ");
		}
		System.out.println("]");
	}

	/**
     * This method generates an int array with the length passed, and the values between 0 to arrayLength*2
     */
    public static int[] genArray(int arrayLength) {
        
        Random rand = new Random();
        int[] array = new int[arrayLength];

        //fill the array with variables up to twice the length of the array itself
        for(int i=0; i<array.length; i++)
            array[i] = rand.nextInt(arrayLength*2);

        return array;
    }
}