/*
 *  Alexander SHmakov
 *  140059
 *  cmpt370::assign3
 *  Nov 13, 2016
 */

/*
    This class represents priority queue nodes and Huffman tree nodes*/
public class Node {

    //priority nodes
	private Node next;
    private Node previous;

    //Huffman nodes
	private Node left;
    private Node right;
    
    //values of the node
	private char character;
    private int count;
    
    //priority queue constructor
	Node(char character, int count) {
        this.count = count;
        this.character = character;
        this.left = null;
        this.right = null;
	}
    //huffman tree node constructor
	Node(Node left, Node right) {
		this(' ', left.getCount()+right.getCount());
        this.left = left;
        this.right = right;
	}

	public int getCount(){
		return this.count;
	}

	public char getChar(){
		return this.character;
	}

    public Node getLeft() {
        return this.left;
    }

    public Node getRight() {
        return this.right;
    }

    public Node getNext() {
        return this.next;
    }

    public Node getPrevious() {
        return this.previous;
    }

    public Node setLeft(Node left) {
        this.left = left;
        return left;
    }

    public Node setRight(Node right) {
        this.right = right;
        return right;
    }

    public Node setNext(Node next) {
        this.next = next;
        return next;
    }
    public Node setPrevious(Node previous) {
        this.previous = previous;
        return previous;
    }

    //used to print the qriority queue
    public String toString() {

        String rs = " " + character + "(" + getCount() + ") ";
        if(getNext() == null)
            rs += "-> Null";
        else rs += "->" + getNext();
        return rs;
    }

    //used to print the huffman tree
    // public String printTree() {

    //     if(getLeft()==null || getRight()==null)
    //         return "";

    //     String rs = " " + count + "{ left:";

    //     if(left != null )
    //         rs += "\n" + getLeft().printTree();

    //     rs += ", right:";

    //     if(right != null)
    //         rs += "\n" + getRight().printTree() + "}\n";
        
    //     return rs;
    // }
}