/*
 *	Alexander SHmakov
 *	140059
 *	cmpt370::assign3
 *	Nov 13, 2016
 */

import java.net.*;
import java.io.*;

public class Frequencies {
	
	//the int[] that will store the frequencies
	private int[] charCounts;

	private URL url;
	private int sum, bits;

	// full constructor
	public Frequencies(URL url, boolean print, int size) {
		//setting initial url to use later
		setUrl(url);
		//will be used to find the original amount of bits needed to store the letters
		this.sum = 0;
		//the maximum amount of bits originally needed to represent each letter (before the encoding)
		this.bits = (int)(Math.log10(size)/Math.log10(2.0) + 1);

		//making an array that will store the character counts, and sets the redirected url
		this.charCounts = getFreq(size);
		//will calculate the original amount of bits needed to store all the letters
		setSum();
		//prints the array
		if(print)
			printResults(getUrl(), size);
	}
	// public Frequencies(URL url, boolean print) {
	// 	this(url, print, 26);
	// }
	// public Frequencies(URL url, int size) {
	// 	this(url, false, size);
	// }
	// public Frequencies(URL url) {
	// 	this(url, false, 26);
	// }

	//used for cases where we provide the frequency array,
	//but want to be able to calculate the original amount of bits to store the letters
	public Frequencies(int[] arr, int size) {
		this.sum = 0;
		this.bits = (int)(Math.log10(size)/Math.log10(2.0) + 1);
		this.charCounts = arr;
		setSum();
	}


	public int getBits() {
		return this.bits;
	}

	public int getSum() {
		return this.sum;
	}

	public int[] getCounts() {
		return this.charCounts;
	}

	public URL getUrl() {
		return this.url;
	}

	public void setUrl(URL url) {
		this.url = url;

	}
	public void addToSum(int newSum) {
		this.sum += newSum;	
	}

	//calculates the ORIGINAL total amount of bits needed to store ALL the characters data
	// then adds it to this.sum  
	public void setSum() {
		int sum = 0;
		for(int i=0; i<getCounts().length; i++) {
			sum += getBits() * getCounts()[i];
		}	
		addToSum(sum);
	}


	/*
		Used to generate the frequency array after reading an entire html page
		input:
			size - and interger that tells what size of array needed 26 or 52
		output:
			the int[] with all the frequencies
	*/
	public int[] getFreq(int size) {
		
		int[] charCounts = new int[size];
		
		//used to avoid the special ASCII characters that are between the upper and
		// lower case letters when storing frequencies to different indexes
		int offset = 6;

		try {

			//the original url that is going to be redirected later
			URL server = getUrl();

			HttpURLConnection webpageConnection= (HttpURLConnection) server.openConnection();
			webpageConnection.setRequestMethod("GET");
			webpageConnection.connect();
			InputStream in = webpageConnection.getInputStream();

			//setting the redirected url
			setUrl(webpageConnection.getURL());

			byte[] buffer = new byte[4096];
			int bytesRead = in.read(buffer);

			while(bytesRead > 0)
			{
				//for every byte, we will check if its in range
				for(int i=0; i<bytesRead; i++) {
					//uppercase range
					if((int)buffer[i] >= 'A' && (int)buffer[i] <= 'Z')
						charCounts[(int)buffer[i]-(int)'A']++;
					//lowercase range
					else if((int)buffer[i] >= 'a' && (int)buffer[i] <= 'z') {
						//if size=52 store in idx 26-51
						if(size>26)
							charCounts[(int)buffer[i]-(int)'A' -offset]++;
						//else  size=26, idx 0-25
						else charCounts[(int)buffer[i]-(int)'a']++;
					}
				}
				//next byte
				bytesRead = in.read(buffer);
			}
		}
		catch(Exception e) { System.out.println("Error opening page\n"+e); }

		return charCounts;
	}


	/*
		This method prints every letter in the array and its frequency
		input:
			url - the url of that page. in some cases we dont need it, since we can provide an array
			size - the size of the array.
	*/
	public void printResults(URL url, int size) {

		//printing out the counts
		if(url != null)
			System.out.println("URL: " + url);
		
		System.out.println("\n\nArray counts:");

		for(int i=0; i<getCounts().length; i++) {
			//if size=52, overwrite the special chars after the uppercase letters with the lowercase
			if(i>=26 && size>26)
				System.out.println(""+(char)(i+(int)'A' +6) + " :"+ getCounts()[i]);
			else System.out.println(""+(char)(i+(int)'A') + " :"+ getCounts()[i]);
		}

	}
}