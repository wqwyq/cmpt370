/*
 *	Alexander SHmakov
 *	140059
 *	cmpt370::assign3
 *	Nov 13, 2016
 */

import java.net.*;
import java.io.*;

/*
	This class is used to approximate the amount of bits needed to store character A-Z and a-z after encoding using Huffman codes
*/
public class Huffman {

	public int maxBitLength, size;

	//freq is an object that stores the frequancies of the characters A-Z and a-z in an int[]
	public Frequencies freq;

	public Huffman(int size) {
		freq = null;
		maxBitLength = 0;
		this.size = size;
	}

	/*This method runs the program*/
	public String run(boolean print) {
		try {
			//making an instance that will find the frequencies of characters at the given page url
			freq = new Frequencies(new URL("https://wikipedia.org/wiki/Special:Random/"), print, size);
			// freq = new Frequencies(new URL("https://en.wikipedia.org/wiki/The_King's_University_(Edmonton)"), print, size);
		}
		catch(Exception e) {
			System.out.println("Cant read page.. try a different URL\n");
		}

		//making an instance that will build a priority queue using the frequencies found by freq
		PriorityQueue que = new PriorityQueue( freq.getCounts() );

		// System.out.println("\n\nThe queue is:\n" + que.getStr());

		/*
			calculates the amount of bits needed after encoding + the binarry sequences that are used for the tree encoding
			buildTree builds a tree based on the priority queue built by que
			then the calcSize will traverse the tree and add the amount of bits needed to store chars of each type (needed amount of bits * the frequency of that char)
			then store the total sum in the newSum
		*/
		int newSum = calcSize(print, buildTree(print, que), 0);
		newSum += 8 + (maxBitLength * size);

		// System.out.println("\nbits:" +freq.getBits()+ "\nOriginal size (bits): "+ freq.getSum() +"\nCompressed size (bits): " + newSum +"\nCompression: "+ (((double)newSum/freq.getSum()) *100) + "%");
		return "" +freq.getSum()+ "," +(((double)newSum/freq.getSum()) *100);
	}

	/*
		This method is used to determine what is the longest bit sequence of the tree
		-finding the depth of the lowest frequency charactor
	*/
	public void findMaxDepth(int depth) {
		if(depth > maxBitLength)
			maxBitLength = depth;
	}

	/*
		This method is used to build the Huffman tree
		input:
			print - a bool that will trigger a function that will print the tree
			que - the priority queue object that will return the head node, and insert trees into the queue
		output:
			a head node that has the entire queue under it
	*/
	public Node buildTree(boolean print, PriorityQueue que) {
		Node head = que.getFirst();
		Node temp = null;
		Node tree = null;

		//if not the end of the queue
		while (head != null) {
			//work with a copy of the head node
			temp = head;
			
			//build the tree while the next node in the queue is not null
			if(head.getNext() != null) {
				//make a parent node that will have the first two node in the queue as its children
				tree = new Node(temp, temp.getNext());
				//insert the new node to the right spot in the queue
				temp = que.insertNode(temp, tree);
				//print the trees if print is true
				// if(print)
				// 	System.out.println("	"+head.toString()+"\n\n"+tree.printTree());
				head = head.getNext().getNext();
				continue;
			}
			//used to print the head node after the tree is built
			// if(print)
			// 	System.out.println(""+head.toString()+"\n"+tree.printTree());
			return head;
		}
		return head;
	}


	/*
		Recources through the Huffman tree, and in each node adds to the total amount of bits the amount of bits needed to store (each type of character) or (the encoded tree) 
		input:
			print - a bool that will print the amount added in each node
			head - the head node of the Huffman tree
			depth - and integer that keeps track of the amount of bits needed to store each type of char. the deeper the character in the tree, the more bits needed
		output:
			return the total amount of bits needed to store
	*/
	public int calcSize(boolean print, Node head, int depth) {
		int sum = 0;	
		//if its node the buttom of the tree
		if(head != null) {
			//if its a node that was part of the original priority queue
			if(head.getChar() != ' ') {
				//print the amount added to the sum
				if(print)
					System.out.println("The current char: "+head.getChar()+"\nhead:"+head.getCount()+"*"+depth+"\n");
				
				//adding the needed bits to store all the letters of that kind to the sum
				sum += head.getCount() * depth;

				//using length of lengths method to encode the tree
				//used to determine the amount of bits needed to encode the paths of each character.
				sum += depth;

				findMaxDepth(depth); 
			}
			//traversing the tree, and adding the sum returned by the children to the parent node's sum
			sum += calcSize(print, head.getLeft(), depth+1);
			sum += calcSize(print, head.getRight(), depth+1);
		}
		return sum;
	}
}