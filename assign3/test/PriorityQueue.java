/*
 *	Alexander SHmakov
 *	140059
 *	cmpt370::assign3
 *	Nov 13, 2016
 */

/*
	This class is used to represent a priority queue
*/
public class PriorityQueue {

	//The first node	
	Node head;
	Node temp;

	//stores the queue is a form of a string
	// String queue = "";

	public PriorityQueue(int[] counts) {

		this.temp = null;
		// inserting nodes into a queue and asigning a charactor for each one
		this.head = build(counts);
		//printing the entire queue to a string 
		// queue += this.head;
	}

	//return the head of the queue
	public Node getFirst() {
		return this.head;
	}

	//return a string with the antire queue
	// public String getStr() {
	// 	return this.queue;
	// }

	/*
		builds the queue using an int[] from a different object
		input:
			counts - an int[] that contains frequencies of (upper) or (upper and lower) case letters
		output:
			the head node of the queue
	*/
	public Node build(int[] counts) {
		//going through each frequency in the array
		for(int i = 0; i < counts.length; i++) {
			//inserting a newly created priorityqueue node where every frequency had a character assigned to it
			temp = insertNode( temp, new Node( (char)(i+(int)'\0') , counts[i] ));
			head = temp;
		}
		return head;
	}

	/*
		inserts a node to the queue
		input:
			head - the first node of the queue
			neaxt - the newly created node that is added
		output:
			the first node of the queue
	*/
	public Node insertNode(Node head, Node next) {
		//if the queue was empty
		if(head == null) {
			head = next;
			return head;
		}

		//the next node has higher priority, set the new node as the next node of the current head node
		if(head.getCount() <= next.getCount()) {
			head.setNext( insertNode(head.getNext(), next) );
		}
		else {
			Node temp = head;
			head = next; 
			head.setNext(temp);
		}
		return head;
	}
}