/*
 *	Alexander SHmakov
 *	140059
 *	cmpt370::assign3
 *	Nov 19, 2016
 */

import java.util.*;
import java.io.*;
import java.nio.file.*;

public class Main {

	/*The main method of the program*/
	public static void main(String[] args) {
		String fileName = "compressions.csv", fileName2 = "URLs.csv";
		int loops = 10000;
		int size = 256;
		// int loops = 20;

		ArrayList<int[]> allArr = new ArrayList<int[]>();
		int[] totalCounts = new int[size];
		String[] allData = new String[loops];
		String temp;

		try {
			Files.write(Paths.get(fileName), "bits, Custom Compression (%), Standard Compression (%)\n".getBytes());
			Files.write(Paths.get(fileName2), ("").getBytes());

			for (int i = 0; i < loops; i++) {
				//generating a new page
				Huffman huff = new Huffman(size);

				//calculating the compression percentage, and storing the results in a string
					// the first bool tells if we need to encode the tree as well as the data
					//the sencond bool is a flag, if true, it will print to terminal
				temp = huff.run(null,null,true, null, false);

				//writing the data to strings
				allData[i] = huff.freq.getSum() + temp;
				System.out.println(""+i+" - " + allData[i]);

				allArr.add(huff.freq.getCounts());

				//store the used URLs is a file
				Files.write(Paths.get(fileName2), (huff.freq.getUrl()+"\n").getBytes(), StandardOpenOption.APPEND);
			}


			//generating the average counts across all the pages
			for (int i=0; i < loops; i++) {
				for (int j=0; j<allArr.get(i).length; j++) {

					//summing up the count of each character
					totalCounts[j] += allArr.get(i)[j];
					//calculates the average count of each character on the last i loop 
					if(i == allArr.size()-1)
						totalCounts[j] = totalCounts[j]/loops;
				}
			}
			
			for (int i = 0; i < loops; i++) {
				//generating a new page
				Huffman huff = new Huffman(size);

				//calculating the compression percentage, and storing the results in a string
					//the bool is a flag, if true, it will print to terminal
				temp = huff.run(allArr.get(i),totalCounts, false, null, false);
				//writing the data to strings
				allData[i] += temp;
				//writing the results to a file
				Files.write(Paths.get(fileName), (allData[i]+"\n").getBytes(), StandardOpenOption.APPEND);
				//printing the data to the terminal
				System.out.println(""+i+" - " + allData[i]);
			}
		}
		catch(Exception e) {
			System.out.println(""+e);
		}
	}
}