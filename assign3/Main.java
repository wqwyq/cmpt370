/*
 *	Alexander SHmakov
 *	140059
 *	cmpt370::assign3
 *	Nov 19, 2016
 */

import java.util.*;
import java.io.*;
import java.nio.file.*;

public class Main {

	/*The main method of the program*/
	public static void main(String[] args) {
		String fileName = "compressions.csv", fileName2 = "URLs.csv";
		int loops = 500000;
		// int loops = 15;

		try {
			Files.write(Paths.get(fileName), "bits,Compression (%)\n".getBytes());
			Files.write(Paths.get(fileName2), ("").getBytes());

			for (int i = 0; i < loops; i++) {
				//generating a new page
				Huffman huff = new Huffman(52);

				//calculating the compression percentage, and storing the results in a string
					//the bool is a flag, if true, it will print to terminal
				String temp = huff.run(false);

				//writing the results to a file
				Files.write(Paths.get(fileName), (temp+"\n").getBytes(), StandardOpenOption.APPEND);
				//printing the data to the terminal
				System.out.println(""+i+" - " + temp);

				//store the used URLs is a file
				Files.write(Paths.get(fileName2), (huff.freq.getUrl()+"\n").getBytes(), StandardOpenOption.APPEND);
			}
		}
		catch(Exception e) {
			System.out.println(""+e);
		}
	}
}