/*
 *	Alexander Shmakov 140059
 *	cmpt370::assignment 1
 *	Sep 20th, 2016
 */

import java.util.*;
import java.io.*;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;

public class Sorters {

	/**
	 * The main method of the function
	 */
	public static void main(String[] args) {

		String fileName = "randomTimes.csv";
		String fileName2 = "sortedTimes.csv";
		String fileName3 = "revSortTimes.csv";

		// int fileNo = 0;
		// File test = new File(fileName);


		//running time start
		long startTime = 0;
		long endTime = 0;

		ArrayList<int[]> origArrays = new ArrayList<int[]>();
		ArrayList<int[]> reverseStore = new ArrayList<int[]>();
		ArrayList<int[]> sortedArrays = new ArrayList<int[]>();

		// fileNo = 0;
		// //makes new files it already existing
		// while(test.exists() && !test.isDirectory()) { 
		// 	fileNo++; 
		// 	String newName = fileName.replaceAll(".csv", "(" + fileNo + ").csv");   
		// 	test = new File(newName);
		// } 
		


// ################# testing ##################

		int jumps = 10000;

		try {
			// PrintWriter writer = new PrintWriter(test);
			Files.write(Paths.get(fileName), "size,mergeSort,quickSort,insertionSort,selectSort,bubbleSort".getBytes());
			//generating arrays to sort
			for (int i = 1; i <= 100; i++) {
				int[] array = genArray(jumps * i);
				origArrays.add(array);
			}

			// writer.println("size,mergeSort,quickSort,insertionSort,selectSort,bubbleSort");
			// System.out.println("rand");
			System.out.println("size,mergeSort,quickSort,insertionSort,selectSort,bubbleSort");


			//##################################
			//#######  Random Arrays  ##########
			//##################################
			

			for(int j =1; j<=100; j++) {

				Files.write(Paths.get(fileName), (""+(j*jumps)+",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("random "+(j*jumps)+",");

				int[] tempArr = origArrays.get(j-1);
				//merge

				startTime = System.currentTimeMillis();
				int[] temp = mergeSort(0, tempArr.length-1, tempArr);
				endTime = System.currentTimeMillis();
				// writer.print("" + (endTime - startTime) + ",");
				Files.write(Paths.get(fileName), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");

				// ##################
				//storing reversed sorted array for future sorts
				reverseStore.add( reverse(temp) );
				//storing sorted arrays for future sorts
				sortedArrays.add(temp);
				// ##################



				tempArr = origArrays.get(j-1);
				//quick

				startTime = System.currentTimeMillis();
				quickSort(false, 0, 0, tempArr.length-1, tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");



				tempArr = origArrays.get(j-1);
				//insertion

				startTime = System.currentTimeMillis();
				insertionSort(tempArr, 0, false);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");



				tempArr = origArrays.get(j-1);
				//selecion

				startTime = System.currentTimeMillis();
				selectSort(tempArr, false);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");
				


				tempArr = origArrays.get(j-1);
				//bubble

				startTime = System.currentTimeMillis();
				bubbleSort(tempArr, false);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName), ("" + (endTime - startTime) + "\n").getBytes(), StandardOpenOption.APPEND);
				System.out.println("" + (endTime - startTime));

			}

			// writer.close();

			// test = new File(fileName2);

			// fileNo = 0;
			//makes new files it already existing
			// while(test.exists() && !test.isDirectory()) { 
			// 	fileNo++; 
			// 	String newName = fileName.replaceAll(".csv", "(" + fileNo + ").csv");   
			// 	test = new File(newName);
			// } 

			// writer = new PrintWriter(test);
			Files.write(Paths.get(fileName2), "size,mergeSort,quickSort,insertionSort,selectSort,bubbleSort".getBytes());

			// writer.println("size,mergeSort,quickSort,insertionSort,selectSort,bubbleSort");
			// System.out.println("sorted");
			System.out.println("size,mergeSort,quickSort,insertionSort,selectSort,bubbleSort");

			// ##################################
			// #######  sorted Arrays  ##########
			// ##################################

			for(int j =1; j<=100; j++) {

				Files.write(Paths.get(fileName2), (""+(j*jumps)+",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("sorted "+(j*jumps)+",");
				// writer.print(""+(j*jumps)+",");
				// System.out.print(""+(j*jumps)+",");


				int[] tempArr = sortedArrays.get(j-1);
				//merge

				startTime = System.currentTimeMillis();
				int[] temp = mergeSort(0, tempArr.length-1, tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName2), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");

				tempArr = sortedArrays.get(j-1);
				//quick

				startTime = System.currentTimeMillis();
				quickSort(false, 0, 0, tempArr.length-1, tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName2), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");


				tempArr = sortedArrays.get(j-1);
				//insertion

				startTime = System.currentTimeMillis();
				insertionSort(tempArr, 0, false);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName2), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");



				tempArr = sortedArrays.get(j-1);
				//selecion

				startTime = System.currentTimeMillis();
				selectSort(tempArr, false);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName2), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");
				


				tempArr = sortedArrays.get(j-1);
				//bubble

				startTime = System.currentTimeMillis();
				bubbleSort(tempArr, false);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName2), ("" + (endTime - startTime) + "\n").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime));

			}			


			// writer.close();

			// test = new File(fileName3);

			// fileNo = 0;
			//makes new files it already existing
			// while(test.exists() && !test.isDirectory()) { 
			// 	fileNo++; 
			// 	String newName = fileName.replaceAll(".csv", "(" + fileNo + ").csv");   
			// 	test = new File(newName);
			// } 


			// writer = new PrintWriter(test);

			// writer.println("size,mergeSort,quickSort,insertionSort,selectSort,bubbleSort");
			// System.out.println("reverse");
			Files.write(Paths.get(fileName3), "size,mergeSort,quickSort,insertionSort,selectSort,bubbleSort".getBytes());
			System.out.println("size,mergeSort,quickSort,insertionSort,selectSort,bubbleSort");
			//##################################
			//######  reverse Arrays  ##########
			//##################################


			for(int j =1; j<=100; j++) {

				Files.write(Paths.get(fileName3), (""+(j*jumps)+",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("reverse " +(j*jumps)+",");

				// writer.print(""+(j*jumps)+",");
				// System.out.print(""+(j*jumps)+",");


				int[] tempArr = reverseStore.get(j-1);
				//merge

				startTime = System.currentTimeMillis();
				int[] temp = mergeSort(0, tempArr.length-1, tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName3), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");


				tempArr = reverseStore.get(j-1);
				//quick

				startTime = System.currentTimeMillis();
				quickSort(false, 0, 0, tempArr.length-1, tempArr);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName3), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");



				tempArr = reverseStore.get(j-1);
				//insertion

				startTime = System.currentTimeMillis();
				insertionSort(tempArr, 0, false);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName3), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");



				tempArr = reverseStore.get(j-1);
				//selecion

				startTime = System.currentTimeMillis();
				selectSort(tempArr, false);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName3), ("" + (endTime - startTime) + ",").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime) + ",");
				


				tempArr = reverseStore.get(j-1);
				//bubble

				startTime = System.currentTimeMillis();
				bubbleSort(tempArr, false);
				endTime = System.currentTimeMillis();
				Files.write(Paths.get(fileName3), ("" + (endTime - startTime) + "\n").getBytes(), StandardOpenOption.APPEND);
				System.out.print("" + (endTime - startTime));

			}			


			// writer.close();

		}
		catch (Exception e) {
			System.out.println(""+e);
		}

	}

	//reverse array
	public static int[] reverse(int[] array) {
		for(int i = 0; i < array.length; i++) {
			int temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - 1] = temp;
		}
		return array;
	}



// ##################################### Insertion Sort  ###################################

	public static int[] insertionSort(int[] array, int lastSortedIndex, boolean print) {

		if(lastSortedIndex == array.length-1)
			return array;

		int temp = 0;
		
		for (int i = lastSortedIndex+1; i>0; i--) {
			
			if(array[i-1] > array[i]) {
				temp = array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				if(print)
					System.out.println(Arrays.toString(array) +"\n");
			}
		}
		
		return insertionSort(array, lastSortedIndex+1, print);
	}


// #################################### Bubble sort  #######################################

/*
 *	This methon is used to sort data using bubble sort
 */

	public static int[] bubbleSort(int[] array, boolean print) {

		//temporary variable used to store one of the array values that is going to be swaped
		int temp = 0;

		//this loop will make sure the methos will treverse through the entire array
		for (int i = array.length-1; i > -1; i--) {
			
			//will ensure that the method will check from the beginning of the array till the last sorted value
			for (int j=0; j<i; j++) {

				//swapping of the values
				if(array[j] > array[j+1]) {
					temp = array[j];
					array[j] = array[j+1];
					array[j+1] = temp;
				}
	
				if(print)
					System.out.println(Arrays.toString(array) +"\n");
			}
		}
		return array;
	}

// ###################################### Merge Sort #######################################

public static int[] mergeSort(int min, int max, int[] array){
	
	if (min >= max) {
		return array;
	}

	int temp = 0;
	int maximum = max;
	int minimum = min;
	int mid = (maximum + minimum)/2;
	int lastMin = 0;
	int firstMax = 0;

	mergeSort(min, mid, array);
	mergeSort(mid+1, max, array);
	
	lastMin = mid;
	firstMax = mid+1;
	
	while ( (firstMax <= maximum) && (min <= lastMin) ) {

		if (array[minimum] < array[firstMax])
			minimum++;
		else {
			temp = array[firstMax];

			for (int i = firstMax- 1; i >= minimum; i--)
				array[i+1] = array[i];

			array[minimum] = temp;

			firstMax++;
			lastMin++;
			minimum++;
		}
	}

	return array;
} 


// ##################################  Quick Sort  #########################################

	/**
	 * recursive function to sort the entire 'tree' array. it takes the entire array, and two indexes, start and end.
	 */
	public static int[] quickSort(boolean print, int depth, int start, int end, int[] array) {
		
		int subIndex = 0;

		//stopping case
		if(start < end) {

			if(print) {
				if(depth == 0) {
					System.out.print("Depth "+depth);
				}
			}

			//getting pivot index 
			subIndex = split(print, depth, array, start, end);

			if(print) {
				System.out.println("\nEntire Array: "+ Arrays.toString(array)+ "\n");
			}

			//left side
			if(print)
				System.out.print("Left Depth "+(depth+1));
			
			quickSort(print, depth + 1, start, subIndex - 1, array);

			if(print)
				System.out.println();
	
			//right side
			if(print)
				System.out.print("Right Depth "+(depth+1));

			quickSort(print, depth + 1, subIndex + 1, end, array);
			
			if(print)
				System.out.println();
		}

		return array;
	}

	/**
	 * this method returns the index on which the array will be 'split'
	 */
	public static int split(boolean print, int depth, int[] array, int start, int end) {

		//used to swap values in the array
		int temp = 0;
		int subIndex = start;
		
		//the middle value that the array will be 'splited' on, which is taken from the end of the array
		int pivot = array[end];


		//goes through the sub-array values
		for(int i = start; i < end; i++) {

			//if value smaller than pivot, swap with a value in the beginning of the array at index 'subIndex'
			if(array[i] <= pivot) {
				temp = array[i];
				array[i] = array[subIndex];
				array[subIndex] = temp;

				subIndex++;
			}
		}

		// places the pivot in the middle of the sub-array
		temp = array[end];
		array[end] = array[subIndex];
		array[subIndex] = temp;

		if(print) {
			//displaying the sorted sub-array	
			System.out.print(": [ ");
			
			for (int i=start; i<=end; i++) {
				System.out.print(array[i]);
				if(i != end)
					System.out.print(", ");
			}
			System.out.println(" ]");
			System.out.println("Pivot: "+ pivot);
		}

		return subIndex;
	}


// #################################  Selection Sort  ######################################

	/**
	 * This method takes an int array and sorts with Selection sort.
	 */
	public static int[] selectSort(int[] array, boolean print) {

		int temp = 0;
		int nextMin = 0;

		if(print)
			System.out.println("Beginning array: " + Arrays.toString(array));

		//passing through the outer loop
		for(int i=0; i<array.length - 1; i++) {
			nextMin = i;
			//passing through the inner loop to see if the next value is smaller then the one in the outer loop
			for(int j = i+1; j<array.length; j++) {

				//taking the smallest value from the upcoming values in the array
				if(array[nextMin] > array[j]) {
					nextMin = j;
				}
			}

			//swaping the next value in the array with the next smallest
			temp = array[i];
			array[i] = array[nextMin];
			array[nextMin] = temp;
			if(print)
				System.out.println("Pass " + (i+1) +": "+ Arrays.toString(array));
		}
		if(print)
			System.out.println("End array: " + Arrays.toString(array));
		return array;
	}



	/**
	 * This method generates an int array with the length passed, and the values between 0 to arrayLength*2
	 */
	public static int[] genArray(int arrayLength) {
		
		Random rand = new Random();
		int[] array = new int[arrayLength];

		//fill the array with variables up to twice the length of the array itself
		for(int i=0; i<array.length; i++)
			array[i] = rand.nextInt(arrayLength*2);

		return array;
	}
}