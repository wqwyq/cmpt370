/*
 *	Alexander Shmakov 140059
 *	cmpt370::lab1
 *	Sep 11th, 2016
 */

import java.util.*;

public class SelectSort {

	/**
	 * The main method of the function
	 */
	public static void main(String[] args) {

		//temp array
		int[] array = {2,4,6,0,1};
		sort(array);

		System.out.println("\nSorting new array:\n");
		//creating new array
		array = genArray(10);
		sort(array);

		return;
	}

	/**
	 * This method takes an int array and returns the same array, but sorted.
	 */
	public static int[] sort(int[] array) {

		int temp = 0;
		int nextMin = 0;

		System.out.println("Beginning array: " + Arrays.toString(array));

		//passing through the outer loop
		for(int i=0; i<array.length - 1; i++) {
			nextMin = i;
			//passing through the inner loop to see if the next value is smaller then the one in the outer loop
			for(int j = i+1; j<array.length; j++) {

				//taking the smallest value from the upcoming values in the array
				if(array[nextMin] > array[j]) {
					nextMin = j;
				}
			}

			//swaping the next value in the array with the next smallest
			temp = array[i];
			array[i] = array[nextMin];
			array[nextMin] = temp;
			System.out.println("Pass " + (i+1) +": "+ Arrays.toString(array));
		}

		System.out.println("End array: " + Arrays.toString(array));
		return array;
	}

	/**
	 * This method generates an int array with the length passed, and the values between 0 to arrayLength*2
	 */
	public static int[] genArray(int arrayLength) {
		
		Random rand = new Random();
		int[] array = new int[arrayLength];

		for(int i=0; i<array.length; i++)
			array[i] = rand.nextInt(arrayLength*2);

		return array;
	}
}