/*
 *	Alexander Shmakov
 *	cmpt370::lab3
 *	Oct 4,2016
 */

import java.util.*;


/*
 *	This class is representing a binary tree
 */
public class BinarySearch {

	/*
	 *	This is the main method of the program
	 */
	public static void main(String[] args) {

		int[] array = null;
		Scanner sc = new Scanner(System.in);
		int nextNode = 0;

		try {
			if(args.length == 0 ) {
				System.out.println("pass amount to programm");
				return;
			}

			array = genArray(Integer.parseInt( args[0] ) );
			System.out.println(Arrays.toString(array));
		}
		catch(Exception e) {
			System.out.println("Error: " + e);
		}

		Node head = null;

		for(int i=0; i<array.length; i++) {
			head = insertNode(false, head, array[i]);
		}

		System.out.println("\n\nTree:\n");
		printTree(head, 0);

		// lab4 start
		System.out.println("\n\nPlease input new nodes");

		while(true) {
			try {
				Thread.sleep(1000);
				System.out.print("Next: ");
				nextNode = sc.nextInt();
				if(nextNode == -1)
					return;

				head = insertNode(true, head, nextNode);
				System.out.println("\n\n");
			}
			catch(InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
			catch(Exception e) {
				System.out.println(""+e);
			}
		}
	}

	/*
	 *	This method is used to generate a binary tree
	 *	uses an array to generate the values for the nodes
	 */
	public static Node insertNode(boolean print, Node first, int newNode) {

		Node t = null;

		if(first == null) {
			first = new Node(newNode);
			// System.out.println("added node: "+newNode);
			if(print)
				System.out.println(""+first.getNumber()+ " -> null -> null");
			// head = first;
			return first;
		}

		if( first.getNumber() == newNode) {
			// System.out.println("skipped node: "+newNode);
			return first;
		}
		
		if( first.getNumber() > newNode) {
			// System.out.println("recurse LEFT");
			t = insertNode(print, first.getLeft(), newNode);
			first.setLeft(t);

			if(print) {
				if(t.getNumber()<newNode) {
					if(first.getLeft().getRight()!=null) {
						System.out.println(""+first.getNumber()+" -> "+ t.getNumber()+" -> "+t.getRight().getNumber());
					}
					else {
						System.out.println(""+first.getNumber()+" -> "+ t.getNumber()+" -> null");
					}
				}
				else {
					if(first.getLeft().getLeft()!=null) {
						System.out.println(""+first.getNumber()+" -> "+ t.getNumber()+" -> "+t.getLeft().getNumber());
					}
					else {
						System.out.println(""+first.getNumber()+" -> "+ t.getNumber()+ " -> null");
					}
					
				}

			}

		}

		else if( first.getNumber() < newNode) {
			// System.out.println("recurse RIGHT");
			t = insertNode(print, first.getRight(), newNode);
			first.setRight(t);
			
			if(print) {
				if(t.getNumber()<newNode) {
					if(first.getRight().getRight()!=null) {
						System.out.println(""+first.getNumber()+" -> "+ t.getNumber()+" -> "+t.getRight().getNumber());
					}
					else {
						System.out.println(""+first.getNumber()+" -> "+ t.getNumber()+" -> null");
					}
				}
				else {
					if(first.getRight().getLeft()!=null) {
						System.out.println(""+first.getNumber()+" -> "+ t.getNumber()+" -> "+t.getLeft().getNumber());
					}
					else {
						System.out.println(""+first.getNumber()+" -> "+ t.getNumber()+ " -> null");
					}
					
				}

			}
		}

		return first;
	}


	public static void printTree(Node head, int depth) {
		if(head != null) {
			System.out.println("Depth " +depth+ ": "+ head);
			printTree(head.getLeft(), depth+1);
			printTree(head.getRight(), depth+1);
		}
		return;
	}


    /**
     * This method generates an int array with the length passed, and the values between 0 to arrayLength*2
     */
    public static int[] genArray(int arrayLength) {
        
        Random rand = new Random();
        int[] array = new int[arrayLength];

        //fill the array with variables up to twice the length of the array itself
        for(int i=0; i<array.length; i++)
            array[i] = rand.nextInt(arrayLength*2);

        return array;
    }
}