/*
 *	This class represents a Node, that has a value, and left and right pointer to decendants
 */

class Node {

    private char character;
    private int count;
    private Node next;
    private Node previous;
    
    public Node(char character, int count) {
        this.count = count;
        this.character = character;
        // this.previous = head;
        this.next = null;
    }
    
    public int getCount(){
        return count;
    }

    public char getChar(){
        return character;
    }

    public Node getNext() {
        return next;
    }

    public Node getPrevious() {
        return previous;
    }

    public Node setNext(Node next) {
        this.next = next;
        return next;
    }
    public Node setPrevious(Node previous) {
        this.previous = previous;
        return previous;
    }

    public String toString() {

        String rs = " " + character + "(" + getCount() + ") ";
        if(getNext() == null)
            rs += "-> Null";
        else rs += "->" + getNext();
        return rs;
    }
}