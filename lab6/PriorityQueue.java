/*
 *	Alexander Shmakov 140059
 *	cmpt370::lab5
 */

public class PriorityQueue {

	public static void main(String[] args) {
		//making an array that will store the character counts
		// Object[] charCounts = new Object[26];
		// char[] chars = {'a','b','','','','','','','','','','','','','','','','','','','','','','','','',};
		

		Node head = null;
		Node temp = null;
		boolean stop = false;
		
		// System.out.println(head);

		System.out.println("The Queue is empty\n-Inserting a,23-");
		temp = insertNode(temp, new Node('a', 23));
		head = temp;
		System.out.println(head);


		System.out.println("There are 1 entries in the queue");
		System.out.println("-Inserting b,24-");
		insertNode(temp, new Node('b', 24));
		head = temp;
		System.out.println(head);


		
		System.out.println("There are 2 entries in the queue");
		System.out.println("-Inserting d,10-");
		temp = insertNode(temp, new Node('d', 10));
		head = temp;
		System.out.println(head);

		System.out.println("There are 2 entries in the queue");
		System.out.println("-Inserting d,15-");
		temp = insertNode(temp, new Node('d', 15));
		head = temp;
		System.out.println(head);


		System.out.println("There are 3 entries in the queue");
		System.out.println("-Removing b,24-");
		temp = removeNode(temp, new Node('b', 24));
		head = temp;
		System.out.println(head);
	}


	public static Node removeNode(Node head, Node node) {
		// 	System.out.println("found the node");
		if(head == null)
			return null;

		// if(head == node) {
		// 	if(head.getNext()==null) {
		// 		head.getPrevious().setNext(null);	
		// 	}
		// 	else head.getPrevious().setNext(head.getNext());
			
		// 	return head;
		// }
		
		// else if (head.getNext() != null) {
		// 	removeNode(head.getNext(),node);
		// }

		// return head;
		Node temp = head;
		head = head.getNext();
		return temp;	

	}

	public static Node insertNode(Node head, Node next) {

		if(head == null) {	//no nodes
			head = next;
			return head;
		}

		//the new node has higher priority
		if(head.getCount() <= next.getCount()) {
			head.setNext( insertNode(head.getNext(), next) );
		}

		else {
			Node temp = head;
			head = next; 
			head.setNext(temp);
		}
		return head;
	}
}