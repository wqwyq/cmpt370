/*
 *	Alexander Shmakov 140059
 *	cmpt370::lab5
 */

/*
	Starting code for CMPT 370 lab five.
	This code opens The King's University (Edmonton) Wikipedia page
	and prints the webpage to the terminal.
*/

import java.net.*;
import java.io.*;

public class ReadPage
{
	public static void main(String[] args)
	{
		//making an array that will store the character counts
		int[] charCounts = new int[26];
		try
		{
			URL server = new URL("https://en.wikipedia.org/wiki/The_King%27s_University_(Edmonton)");
            HttpURLConnection webpageConnection= (HttpURLConnection) server.openConnection();
            webpageConnection.setRequestMethod("GET");
			webpageConnection.connect();
			InputStream in = webpageConnection.getInputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = in.read(buffer);

			while(bytesRead > 0)
			{
				//for every byte, we will check if its in range
				for(int i=0; i<bytesRead; i++) {
					//uppercase range
					if((int)buffer[i] >= 'A' && (int)buffer[i] <= 'Z')
						charCounts[(int)buffer[i]-(int)'A']++;
					//lowercase range
					else if((int)buffer[i] >= 'a' && (int)buffer[i] <= 'z')
						charCounts[(int)buffer[i]-(int)'a']++;					
				}
				//next byte
				bytesRead = in.read(buffer);
			}

			//printing out the counts
			System.out.println("Array counts:");
			for(int i=0; i<charCounts.length; i++)
				System.out.println(""+(char)(i+(int)'A')+" :"+charCounts[i]);	
			
		}
		catch(Exception e) { System.out.println("Error opening page\n"+e); }
	}
}