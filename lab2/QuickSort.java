/*
 *	Alexander Shmakov 140059
 *	cmpt370::lab2
 *	Sep 18th, 2016
 */

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * The programm is used to perfor a quick sort of an integer array
 */
public class QuickSort {

	/**
	 * The main method of the programm
	 */
	public static void main(String[] args) {

		//running time start
		long startTime = 0;

		//temp array
		int[] lengths = {10, 100000, 500000, 1000000, 10000000};

		for(int i=0; i< lengths.length; i++) {

			System.out.print("\n\n"+lengths[i]+" long array:\n");

			int[] array = genArray(lengths[i]);
			
			
			startTime = System.currentTimeMillis();
			
			if(lengths[i]<100) {
				//array before sorting
				System.out.println("Initial Array: "+ Arrays.toString(array)+"\n");
				//array after sorting
				System.out.println("\nSorted Array: "+Arrays.toString( sort(true, 0, 0, array.length - 1, array) ));
			}
			else
				sort(false, 0, 0, array.length - 1, array);
			
			//displaying running time
			long endTime = System.currentTimeMillis();
			System.out.println("\nRunning time: " + (endTime - startTime) + " Millisecond(s)");

			System.out.println("The array " + (isSorted(array) ? "IS" : "IS NOT") + " sorted!");
 		}

		return;
	}

	/**
	 * recursive function to sort the entire 'tree' array. it takes the entire array, and two indexes, start and end.
	 */
	public static int[] sort(boolean print, int depth, int start, int end, int[] array) {
		
		int subIndex = 0;

		//stopping case
		if(start < end) {

			if(print) {
				if(depth == 0) {
					System.out.print("Depth "+depth);
				}
			}

			//getting pivot index 
			subIndex = split(print, depth, array, start, end);

			if(print) {
				System.out.println("\nEntire Array: "+ Arrays.toString(array)+ "\n");
			}

			//left side
			if(print)
				System.out.print("Left Depth "+(depth+1));
			
			sort(print, depth + 1, start, subIndex - 1, array);

			if(print)
				System.out.println();
	
			//right side
			if(print)
				System.out.print("Right Depth "+(depth+1));

			sort(print, depth + 1, subIndex + 1, end, array);
			
			if(print)
				System.out.println();
		}

		return array;
	}

	/**
	 * this method returns the index on which the array will be 'split'
	 */
	public static int split(boolean print, int depth, int[] array, int start, int end) {

		//used to swap values in the array
		int temp = 0;
		int subIndex = start;
		
		//the middle value that the array will be 'splited' on, which is taken from the end of the array
		int pivot = array[end];


		//goes through the sub-array values
		for(int i = start; i < end; i++) {

			//if value smaller than pivot, swap with a value in the beginning of the array at index 'subIndex'
			if(array[i] <= pivot) {
				temp = array[i];
				array[i] = array[subIndex];
				array[subIndex] = temp;

				subIndex++;
			}
		}

		// places the pivot in the middle of the sub-array
		temp = array[end];
		array[end] = array[subIndex];
		array[subIndex] = temp;

		if(print) {
			//displaying the sorted sub-array	
			System.out.print(": [ ");
			
			for (int i=start; i<=end; i++) {
				System.out.print(array[i]);
				if(i != end)
					System.out.print(", ");
			}
			System.out.println(" ]");
			System.out.println("Pivot: "+ pivot);
		}

		return subIndex;
	}

	/**
	 * This method generates an int array with the length passed, and the values between 0 to arrayLength*2
	 */
	public static int[] genArray(int arrayLength) {
		
		Random rand = new Random();
		int[] array = new int[arrayLength];

		for(int i=0; i<array.length; i++) {
		
			array[i] = rand.nextInt(arrayLength*2);
		}
		return array;
	}

	public static boolean isSorted(int[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			if (array[i] > array[i+1]) {
				return false;
			}
		}
		return true;
	}
}